/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wed;

import entidades.BaseDeDatos;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Yanina
 */
@WebServlet(name = "MercaderiaNueva", urlPatterns = {"/MercaderiaNueva"})
public class MercaderiaNueva extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        System.out.println("probando, probando" );
        System.out.println("Producto: " + request.getParameter("nombre"));
        System.out.println("Precio: " + request.getParameter("precio"));
        System.out.println("Foto: " + request.getParameter("foto"));
        System.out.println("Stock: " + request.getParameter("stock"));
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        {
            Producto insertarMercaderia = new Producto();
            insertarMercaderia.nombre = request.getParameter("nombre");
            insertarMercaderia.precio = request.getParameter("precio");
            insertarMercaderia.foto =request.getParameter("foto");
            insertarMercaderia.stock = request.getParameter("stock");
            insertarMercaderia.validarFoto();

            String consultaSQL = "INSERT INTO `almacén` (`nombre`, `precio`, `foto`, `stock`) VALUES "
                    + "(" + "'" + insertarMercaderia.nombre + "' " + ","
                    + "" + "'" + insertarMercaderia.precio + "' " + ","
                    + "" + "'" + insertarMercaderia.foto + "'" + ","
                    + "" + "'" + insertarMercaderia.stock + "'" + " );";
            System.out.println(insertarMercaderia);
            out.println("Aca vas a subir el producto nuevo...");
            Connection miConexion = null;
            try {
                miConexion = BaseDeDatos.getInstance().getConnection();
                PreparedStatement miPreparativo = miConexion.prepareStatement(consultaSQL);
               miPreparativo.execute();
                out.println("Producto agregado");
            } catch (ClassNotFoundException miExcepcion) {
                System.out.println(miExcepcion);
            } catch (SQLException miExcepcion) {
                System.out.println(miExcepcion);
            } finally {
                try {
                    if (miConexion != null) {
                        miConexion.close();
                    }

                } catch (SQLException miExcepcion) {
                    System.out.println(miExcepcion);
                }
            }

        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
