/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wed;

import entidades.BaseDeDatos;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Yanina
 */
@WebServlet(name = "ConsultaSuper", urlPatterns = {"/ConsultaSuper"})
public class ConsultaSuper extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        response.setContentType("text/html;charset=UTF-8");
        out.println("<!DOCTYPE html>\n"
                + "<html>\n"
                + "    <head>        \n"
                + "        <meta charset=\"UTF-8\">\n"
                + "        <meta http-equiv=\"X-UA-Compatible\"coment=\"IE=edge\">\n"
                + "        <title>Listado de Producto</title>\n"
                + "        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n"
                + " <link rel=\"stylesheet\" type=\"text/css\" media=\"screen\" href=\"css/productos.css\">"
                + "        <script src=\"main.js\"></script>\n"
                + "    </head>\n"
                + "    <body>\n"
                + "        <header></header>\n"
                + "       <main>"
                + "<div class=\"wrap category-page\" ng-controller=\"Application.Controllers.Category.ProductList\">");
        {
            String consultaSQL = "SELECT * FROM `almacén`ORDER BY id DESC LIMIT 6 ";

            Connection miConexion = null;
            try {
                miConexion = BaseDeDatos.getInstance().getConnection();
                PreparedStatement miPreparativo = miConexion.prepareStatement(consultaSQL);
                ResultSet miResultado = miPreparativo.executeQuery();
                while (miResultado.next()) {
                    Producto miProducto = new Producto();
                    miProducto.nombre = miResultado.getString("nombre");
                    miProducto.precio = miResultado.getString("precio");
                    miProducto.foto = miResultado.getString("foto");
                    miProducto.stock = miResultado.getString("stock");
                    System.out.println(miProducto);

                    /*  out.println("<p>" + miProducto.nombre + " "
                            + miProducto.precio + " "
                            + miProducto.stock + " "
                            + "<img src=" + miProducto.foto + "alt=\"Trulli\" width=\"100\" > "
                            + "</p>");
                    
                    
                   out.println("<article>");    
                    out.println( "<img src=' " + miProducto.foto +" 'width=\"100\">");
                    out.println("<div>Producto: "+  miProducto.nombre +" </div>");
                    out.println("<div>Precio: "+ miProducto.precio +"</div>");
                    out.println("<div>stock: "+ miProducto.stock+"</div>");
                     out.println("</article>");*/
                    out.println("<div class=\"products\">\n"
                            + "        <div class=\"product-box\"\n"
                            + "             ng-repeat=\"product in products\">\n"
                            + "          <div class='title'>" + miProducto.nombre + "</div>\n"
                            + "          <div class='show-base'>\n"
                            + "             <img src=\"" + miProducto.foto + "\" alt=\"{{product.name}}\" />"
                            + "            <div class=\"mask\">\n"
                            + "              <h2>" + miProducto.nombre + "</h2>\n"
                            + "              <p ng-class=\"{old:product.specialPrice}\">$" + miProducto.precio + "</p>\n"
                            + "              <p ng-show=\"product.specialPrice\">Stock " + miProducto.stock + "</p>\n"
                            + "              <div>\n"
                            + "                <a href=\"#\" class=\"tocart\">COMPRAR</a>\n"
                            + "              </div>\n"
                            + "            </div>\n"
                            + "          </div>\n"
                            + "         </div>\n"
                            + "        </div>");

                }

                out.println("</div>\n");
            } catch (ClassNotFoundException miExcepcion) {
                System.out.println(miExcepcion);
            } catch (SQLException miExcepcion) {
                System.out.println(miExcepcion);
            } finally {
                try {
                    if (miConexion != null) {
                        miConexion.close();
                    }

                } catch (SQLException miExcepcion) {
                    System.out.println(miExcepcion);
                }
            }
            out.println(" </main>\n"
                    + "     <footer> </footer>\n"
                    + "    </body>\n"
                    + "</html>");
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
