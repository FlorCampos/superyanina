
package wed;


public class Producto {
    String nombre;
    String precio;
    String foto;
    String stock;
    
    void validarFoto(){
        if (this.foto == null ||this.foto.isEmpty()) {
            //la foto que cargo el usurio
            this.foto ="https://static.vecteezy.com/system/resources/previews/000/566/937/non_2x/vector-person-icon.jpg" ;
        } 
            
        
    }
    @Override
    public String toString() {
        return "Producto{" + "nombre=" + nombre + ", precio=" + precio + ", foto=" + foto + ", stock=" + stock + '}';
    }
    
}
